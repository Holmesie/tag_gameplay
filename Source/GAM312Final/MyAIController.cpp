// Fill out your copyright notice in the Description page of Project Settings.

//the pathing set up in this file uses a navigation mesh and various target points. The navigation mesh intereacts with the pawns movement component in ways I do not fully understand, but it knows where is is able to go based on collision detection. 
//The target points are all loaded into an array and then chosen from randomly on a timer to make the pawn seem a bit more natural, and to give the player a chance to catch up. 


#include "MyAIController.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

//we begin play by getting any waypoints that have been set, then assigning a random one for the object to go to.
void AMyAIController::BeginPlay() {
	Super::BeginPlay();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), Waypoints);
	GoToRandomWaypoint();
}
//indexing an array of waypoints for the AI to access. 
ATargetPoint* AMyAIController::GetRandomWaypoint() {
	auto index = FMath::RandRange(0, Waypoints.Num() - 1);
	return Cast<ATargetPoint>(Waypoints[index]);
}
//Here we tell our AI to move once a destination has been reached. 
void AMyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) {
	Super::OnMoveCompleted(RequestID, Result);
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AMyAIController::GoToRandomWaypoint, 1.0f, false);
}
//This is what will move out actor, a simplie system that requires declared waypoints. 
void AMyAIController::GoToRandomWaypoint() {
	MoveToActor(GetRandomWaypoint());
}
